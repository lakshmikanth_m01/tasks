import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NameListComponent } from './components/name-list/name-list.component';
import { NameDetailComponent } from './components/name-detail/name-detail.component';

const routes: Routes = [
  {
    path: 'list',
    component: NameListComponent
  },
  {
    path: 'details',
    component: NameDetailComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'list'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
