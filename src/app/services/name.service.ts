import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NameService {

  constructor(private http: HttpClient) { }

  public getData<T>(url: string): Observable<T> {
    return this.http.get<T>(url);
  }
}
