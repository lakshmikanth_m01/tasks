import { Component, OnInit } from '@angular/core';
import { NameService } from 'src/app/services/name.service';

@Component({
  selector: 'app-name-list',
  templateUrl: './name-list.component.html',
  styleUrls: ['./name-list.component.css']
})
export class NameListComponent implements OnInit {

  constructor(private nameService: NameService) { }

  ngOnInit() {
    this.nameService.getData('http://localhost:7627/customerDetails').subscribe(
      (response) => {
        console.log(response);
      });
  }

}
