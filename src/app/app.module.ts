import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NameListComponent } from './components/name-list/name-list.component';
import { NameDetailComponent } from './components/name-detail/name-detail.component';
import { NameService } from './services/name.service';

@NgModule({
  declarations: [
    AppComponent,
    NameListComponent,
    NameDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [NameService],
  bootstrap: [AppComponent]
})
export class AppModule { }
