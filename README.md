# Task

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.2.

## Development server
1) Do npm install
2) DO npm run quick 
    Angular application is hosted on `http://localhost:4200/`
    The app uses a JSON server for a backend by default and hosted on `http://localhost:7627/`
    Customer Details API Endpoint - http://localhost:7627/customerDetails
    Account Details API Endpoint - http://localhost:7627/accountDetails


## Application Overview
1) This Application should makes two API Calls to get Customer Details and Account Details API and 
display the details in the Table on LIST page and once user click on customer ID, we should show the customer details

## Tasks

